# cryptsetup-nuke-password

Installing this package lets you configure a special "nuke password" that
can be used to destroy the encryption keys required to unlock the encrypted
partitions. This password can be entered in the usual early-boot prompt
asking the passphrase to unlock the encrypted partition(s).

This provides a relatively stealth way to make your data unreadable in
case you fear that your computer is going to be seized.

## How can I configure the nuke password

After having installed the package, just run “dpkg-reconfigure
cryptsetup-nuke-password”. Behind the scene, this creates
`/etc/cryptsetup-nuke-password/password_hash` with
the output of `echo your-password |
/usr/lib/cryptsetup-nuke-password/crypt --generate` and rebuilds
the initramfs (`update-initramfs -u`).

## How does it work?

The packages diverts /lib/cryptsetup/askpass by our own script that will
call the original tool to get the password but that will do its own
treatment before outputting the same password to stdout...

To identify the partition being unlocked, we rely on the environment
variables exported by
/usr/share/initramfs-tools/scripts/local-top/cryptroot

To destroy the encryption keys, we call `cryptsetup erase <device>`.

## How to configure the nuke password non-interactively

You need to preseed the password in the debconf database and then
run dpkg-reconfigure in a way where it is not allowed to ask question:
```
$ sudo debconf-set-selections <<END
cryptsetup-nuke-password cryptsetup-nuke-password/password string Th3Pa$$w0rd
cryptsetup-nuke-password cryptsetup-nuke-password/password-again string Th3Pa$$w0rd
END
$ sudo dpkg-reconfigure -f noninteractive cryptsetup-nuke-password
```

## Backing up the encryption key

If you expect to make use of this nuke password, then you should consider
backing up the luks header. That way, if your computer is seized, and then
later given back to you, you will be able to restore the luks headers and
get back access to your data.

Here's the command to use (replace `<device>` with the path of the device
file representing your luks encrypted partition, and `<your-backup-file>`
with the path to the backup file to create):
```
$ sudo cryptsetup luksHeaderBackup <device> --header-backup-file <your-backup-file>
```

To later restore the header, you will have to do:
```
$ sudo cryptsetup luksHeaderRestore <device> --header-backup-file <your-backup-file>
```

## Limitations

Due to the way this feature is implemented, it can't be used if you
configure cryptsetup to use something else than an interactive password
to unlock the encryption keys.

It is also not usable on a live image (unless you configured the nuke
password with debconf preseeding at the time you generated the live image)
since it needs to regenerate the initrd to embed itself and the nuke
password.
