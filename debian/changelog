cryptsetup-nuke-password (6) unstable; urgency=medium

  * Team upload, acked by Raphaël.
  * Upload to unstable.

 -- Helmut Grohne <helmut@subdivi.de>  Sun, 10 Mar 2024 14:11:10 +0100

cryptsetup-nuke-password (5) experimental; urgency=medium

  * Team upload, acked by Raphaël.

  [ Raphaël Hertzog ]
  * Request update of initramfs when nuke password is changed with
    dpkg-reconfigure.

  [ Helmut Grohne ]
  * Upgrade cryptsetup-bin dependency to cryptsetup, as that contains askpass.
  * DEP17: Move files to /usr (M2) and mitigate file loss with diverions (P7).
    (Closes: #1060269)

 -- Helmut Grohne <helmut@subdivi.de>  Fri, 05 Jan 2024 18:53:10 +0100

cryptsetup-nuke-password (4+nmu1) unstable; urgency=medium

  * Non-maintainer upload.
  * Update po file into Brazilian Portuguese translation. (Closes: #1026408)
  * Update po file into Italian translation. (Closes: #1017486)
  * Update po file into Portuguese translation. (Closes: #1028253)
  * debian/control
    + Bump Standards-Version 4.6.2

 -- Paulo Henrique de Lima Santana (phls) <phls@debian.org>  Mon, 19 Jun 2023 23:00:28 -0300

cryptsetup-nuke-password (4) unstable; urgency=medium

  * d/tests/control: Change restriction for askpass autopkgtest from
    `isolation-container` to `isolation-machine`. The test needs to load
    kernel modules (dm_crypt). (Closes: #1007237)
  * d/control: Bump standards-version to 4.6.1, no changes required.
  * d/po/de.po: Add german translation, thanks to Hermann-Josef Beckers.
    (Closes: #1007933)
  * d/po/es.po: Add spanish translation, thanks to Camaleón.
    (Closes: #1008527)
  * d/po/nl.po: Add dutch translation, thanks Frans Spiesschaert.
    (Closes: #1010035)

 -- Jonas Meurer <jonas@freesources.org>  Sat, 16 Jul 2022 22:00:36 +0100

cryptsetup-nuke-password (3) unstable; urgency=medium

  * Initial upload to Debian. (Closes: #1004555)
  * Makefile: Change `LDFLAGS` to `LDLIBS`. Ffixes compilation of crypt.c.
  * crypt.c: Fix detection of invalid hashs.
  * d/control:
    * Bump debhelper compat version to 13.
    * Bump standards-version to 4.6.0, no changes required.

 -- Jonas Meurer <jonas@freesources.org>  Sun, 30 Jan 2022 17:16:45 +0100

cryptsetup-nuke-password (2) kali-dev; urgency=medium

  * Allow preseeding and non-interactive configuration of the nuke password.

 -- Raphaël Hertzog <raphael@offensive-security.com>  Fri, 05 Jul 2019 15:22:51 +0200

cryptsetup-nuke-password (1) kali-experimental; urgency=medium

  * Initial Release.

 -- Raphaël Hertzog <raphael@offensive-security.com>  Fri, 07 Jun 2019 16:14:37 +0200
